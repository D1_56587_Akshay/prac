const mysql = require('mysql2')

const openConnection = () => {
    const connection = mysql.createConnection({

        host: 'mydb',
        user: 'root',
        password: 'root',
        database: 'sdm',
    })

    connection.connect()

    return connection
}

module.exports = {
    openConnection,
}