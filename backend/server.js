const express = require('express')
const db = require('./db')
//const cors = require('cors')


const router = express()

//router.use(cors('*'))

router.use(express.json())

router.get('/', (request, response) => {
    response.send('Hello')
  })

router.get('/all', (request, response) => {
    const statement = `select * from emp`
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if (error) {
            response.send(error);
        } else {
            response.send(result);
        }
    })
})

router.post('/add', (request, response) => {
    const { empid, name, salary, age } = request.body;

    const connection = db.openConnection()

    const statement = `insert into emp (empid, name, salary, age) values (${empid},'${name}',${salary},${age})`

    connection.query(statement, (error, result) => {
        connection.end()
        if (error) {
            response.send(error)
        } else {
            response.send(result)
        }
    })
})

router.patch('/update/:empid', (request, response) => {
    const { empid } = request.params
    const { salary } = request.body

    const statement = ` update emp set salary = ${salary} where empid = ${empid}` 
     const connection = db.openConnection()
     connection.query(statement, (error, result) => {
         connection.end()
         if(error) {
             response.send(error)
         } else {
             response.send(result)
         }
     })
})

router.delete('/delete/:empid', (request, response) => {
    const { empid } = request.params

    const statement = `delete from emp where empid = ${empid}`
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if(error) {
            response.send(error)
        } else {
            response.send(result)
        }
    })
})

router.listen(4000, () => {
    console.log('server started on port 4000')
})
