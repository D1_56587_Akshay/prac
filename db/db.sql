create table emp (
    empid integer primary key,
    name varchar(30),
    salary decimal(8,2),
    age integer
);


INSERT INTO emp VALUES(1, 'akshay', 60000.00, 24);
INSERT INTO emp VALUES(2, 'kuldeep', 50000.00, 25);
INSERT INTO emp VALUES(3, 'ravi', 40000.00, 26);
INSERT INTO emp VALUES(4, 'ankit', 70000.00, 27);
INSERT INTO emp VALUES(5, 'mayur', 80000.00, 30);
INSERT INTO emp VALUES(6, 'lord', 100000.00, 28);
INSERT INTO emp VALUES(7, 'parv', 30000.00, 29);


